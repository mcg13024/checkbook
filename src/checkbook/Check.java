package checkbook;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Check implements CheckInterface {


    Scanner sc = new Scanner(System.in);

    static ArrayList transactions = new ArrayList();
    public static double balance = 0;

    public double deposit_amount, withdraw_amount;

    public static double main_bal;

    public static double ArrayMainBal;




    public void Deposit(){
        System.out.println("Enter Amount");
        deposit_amount = sc.nextDouble();
        balance = deposit_amount + balance;
        double depositBalanceForArray = balance;
        transactions.add(depositBalanceForArray);
        System.out.println("Balance: " + balance);

        check();

    }


    public void Withdraw(){
        System.out.println("Enter Amount");
        withdraw_amount = sc.nextDouble();
        balance = balance - withdraw_amount;

        if (withdraw_amount > balance){
            System.out.println("Insufficient funds");
            check();

        }else if(balance != 0.0){

            System.out.println("Balance: " + balance);
            double withdrawalBalance = balance;
            transactions.add(withdrawalBalance);

        }
        else{

            System.out.println("Account Empty");
            exit(0);
        }
        check();
    }



    public static void main(String[] args) {

        check(); }
    public static void check() {
        Scanner sc = new Scanner(System.in);
        Check objCheck = new Check();
        System.out.println("To deposit into your account enter either 'Deposit' or 'd'\n" +
                "to withdraw from your account enter either 'Withdraw' or 'w'");
        String choice = sc.nextLine();

        if(choice.matches("[0-9]+"))  {
            System.out.println("String expected");
            System.out.println("Try again");
            check();
        }

        if(choice.equalsIgnoreCase("deposit")){
            objCheck.Deposit();
            check();
        }else if(choice.equalsIgnoreCase("d")){
                objCheck.Deposit();
                check();
        }else if(choice.equalsIgnoreCase("withdraw")){
            objCheck.Withdraw();
            check();
        }else if(choice.equalsIgnoreCase("w")){
            objCheck.Withdraw();
            check();
        } else if(choice.equalsIgnoreCase("quit")){
            objCheck.Quit(transactions);

        }else {
            System.out.println("Invalid selection");
            System.out.println("Try again");
            check();
        }
    }

    public void Quit(ArrayList obj) {
        System.out.println("the value in the array is");
        try {
            saveToFile(obj,"check .txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Check.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Check.class.getName()).log(Level.SEVERE, null, ex);
        }
        exit(0);

    }
    public static void saveToFile(ArrayList<Double> transactionsValues, String fileName) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(fileName);

        for(Double Line : transactionsValues)
        {
            writer.println( "Balance is " + Line);
        }
        writer.close();

        try {
            readFromFile();
        } catch (IOException ex) {
            Logger.getLogger(Check.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public static void readFromFile() throws IOException{
        Path filePath = Paths.get("checkbook.txt");
        try(BufferedReader reader = Files.newBufferedReader(filePath,Charset.defaultCharset())){
            String Line = null;
            while((Line = reader.readLine()) != null){
                System.out.println(Line);
            }

        }

    }
}
